package com.babestudios.base.android.ext

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.showKeyboard() {
	view?.let { activity?.showKeyboard(it) }
}

fun Fragment.hideKeyboard() {
	view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
	hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Activity.showKeyboard() {
	showKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Context.hideKeyboard(view: View?) {
	view?.let {
		val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
		inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
	}
}

/**
 * TODO Improvements (remove context, use window focus and ViewTreeObserver):
 * https://developer.squareup.com/blog/showing-the-android-keyboard-reliably/
 */
fun Context.showKeyboard(view: View?) {
	view?.let {
		val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
		inputMethodManager.showSoftInput(it, InputMethodManager.SHOW_IMPLICIT)
	}
}
