package com.babestudios.base.android.ext

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat

@Suppress("unused")
fun Int.getBitmap(context: Context): Bitmap? {
	return when (val drawable = ContextCompat.getDrawable(context, this)) {
		is BitmapDrawable -> BitmapFactory.decodeResource(context.resources, this)
		is VectorDrawableCompat -> (drawable as VectorDrawableCompat?)?.getBitmap()
		else -> throw IllegalArgumentException("unsupported drawable type")
	}
}

fun VectorDrawableCompat.getBitmap(): Bitmap {
	val bitmap = Bitmap.createBitmap(this.intrinsicWidth, this.intrinsicHeight, Bitmap.Config.ARGB_8888)
	val canvas = Canvas(bitmap)
	this.setBounds(0, 0, canvas.width, canvas.height)
	this.draw(canvas)
	return bitmap
}

fun VectorDrawableCompat.tintVectorDrawableCompat(context: Context, colorId: Int): Drawable {
	this.setTint(ContextCompat.getColor(context, colorId))
	return this
}

@Suppress("unused")
fun Int.tintDrawable(context: Context, colorId: Int): Drawable? {
	val drawable = ResourcesCompat.getDrawable(context.resources, this, null)
	return drawable?.tintDrawable(context, colorId)
}

@Suppress("MemberVisibilityCanBePrivate")
fun Drawable.tintDrawable(context: Context, colorId: Int): Drawable {
	val drawable2 = DrawableCompat.wrap(this)
	DrawableCompat.setTint(drawable2.mutate(), ContextCompat.getColor(context, colorId))
	return this
}