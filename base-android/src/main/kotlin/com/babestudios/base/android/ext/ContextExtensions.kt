package com.babestudios.base.android.ext

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Rect
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.WindowManager
import androidx.annotation.AttrRes
import androidx.core.content.ContextCompat

fun Context.color(res: Int): Int = ContextCompat.getColor(this, res)

fun Context.colorStateList(res: Int): ColorStateList = ColorStateList.valueOf(this.color(res))

fun Context.resolveAttribute(@AttrRes attributeResId: Int): Int {
	val typedValue = TypedValue()
	if (this.theme.resolveAttribute(attributeResId, typedValue, true)) {
		return typedValue.data
	}
	throw IllegalArgumentException(this.resources.getResourceName(attributeResId))
}

fun Context.getScreenSize(): Rect {
	val windowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
	return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
		windowManager.currentWindowMetrics.bounds
	} else {
		val displayMetrics = DisplayMetrics()
		@Suppress("DEPRECATION")
		windowManager.defaultDisplay.getMetrics(displayMetrics)
		Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
	}
}
