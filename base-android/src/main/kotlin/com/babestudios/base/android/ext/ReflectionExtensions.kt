package com.babestudios.base.android.ext

import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

/**
 * This function makes a private function accessible and callable for any class through reflection, useful in tests
 */
inline fun <reified T> T.callPrivateFunc(name: String, vararg args: Any?): Any? =
	T::class
		.declaredMemberFunctions
		.firstOrNull { it.name == name }
		?.apply { isAccessible = true }
		?.call(this, *args)

/**
 * This function makes a private field accessible for any class through reflection, useful in tests
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified T : Any, R> T.getPrivateProperty(name: String): R? =
	T::class
		.memberProperties
		.firstOrNull { it.name == name }
		?.apply { isAccessible = true }
		?.get(this) as? R