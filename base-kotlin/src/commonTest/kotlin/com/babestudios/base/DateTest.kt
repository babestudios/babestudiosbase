package com.babestudios.base

import com.babestudios.base.kotlin.ext.SHORT_DATE
import com.babestudios.base.kotlin.ext.parseDate
import com.babestudios.base.kotlin.ext.toDateString
import com.babestudios.base.kotlin.ext.toTimeString
import io.kotest.matchers.shouldBe
import korlibs.time.DateFormat
import korlibs.time.DateTime
import korlibs.time.DateTimeTz
import korlibs.time.parse
import kotlin.test.Test

class DateTest {

	@Test
	fun parseIsoTest() {
		val isoDate = "2019-09-01"

		val date = DateTime(isoDate.parseDate()!!).date

		date.year shouldBe 2019
		date.month.index1 shouldBe 9
		date.day shouldBe 1
	}

	@Test
	fun formatTest() {
		val isoDate = "2019-09-01"
		val isoDate2 = "2019-09-11"

		val date = isoDate.parseDate()
		val date2 = isoDate2.parseDate()

		val formatted = date?.toDateString(SHORT_DATE)
		val formatted2 = date2?.toDateString(SHORT_DATE)
		formatted shouldBe "1 Sep 2019"
		formatted2 shouldBe "11 Sep 2019"
	}

	@Test
	fun formatShortTimeTest() {

		val dateFormat = DateFormat("dd MMM yyyy")
		val date: DateTimeTz = dateFormat.parse("08 Sep 2018")

		val formatted = date.utc.unixMillisLong.toTimeString()
		formatted shouldBe "00:00:00"
	}

}