package com.babestudios.base

//import org.junit.Test
import kotlin.test.Test

/**
 * I created these tests to try out and compare some Kotlin functionalities
 *
 */
class KotlinUnitTest {

	/**
	 * All function apply the given binary operation to the sequence of values.
	 *
	 * Fold and scan will take an initial value.
	 *
	 * Fold returns the final result, while scan returns all intermediate results.
	 *
	 * Reduce and runningReduce take the first value as initial value,
	 * if the return value is the same as the items.
	 *
	 * Reduce (like fold) returns the final result,
	 * while runningReduce (like scan) returns all intermediate results.
	 */

	@Test
	@Throws(Exception::class)
	fun scanFunctionsCompare() {
		val ints = (1..4).asSequence()
		println("fold: ${ints.fold(0) { acc, elem -> acc + elem }}")

		val sequence = ints.scan(0) { acc, elem -> acc + elem }
		println("scan: ${sequence.toList()}")

		println("reduce: ${ints.reduce { acc, elem -> acc + elem }}")

		val sequence2 = ints.runningReduce { acc, elem -> acc + elem }
		println("scanReduce: ${sequence2.toList()}")

	}

	/**
	 * Zip returns a sequence of pairs or a transformed sequence, as long as the two sequences
	 * can be paired.
	 *
	 * ZipWithNext returns a sequence of pairs or a transformed sequence based on
	 * adjacent elements in the sequence.
	 */
	@Test
	@Throws(Exception::class)
	fun zipFunctionsCompare() {
		val ints = (1..4).asSequence()
		val values = intArrayOf(1, 5, 6, 9, 15, 43).asSequence()

		val sequence = values.zip(ints)
		println("zip pairs: ${sequence.toList()}")

		val sequence2 = values.zip(ints) { a, b -> a - b }
		println("zip transform: ${sequence2.toList()}")

		val sequence3 = values.zipWithNext { a, b -> b - a }
		println("zipWitNext: ${sequence3.toList()}")

	}
}
