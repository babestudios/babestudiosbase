package com.babestudios.base.kotlin.api

interface AnalyticsContract {
	fun logAppOpen()
	fun logScreenView(screenName: String)
	fun logSearch(queryText: String)
}