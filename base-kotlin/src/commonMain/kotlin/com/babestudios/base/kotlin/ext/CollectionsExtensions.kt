package com.babestudios.base.kotlin.ext

fun <K, V : Any> Map<K, V?>.toVarargArray(): Array<out Pair<K, V>> = map { Pair(it.key, it.value!!) }.toTypedArray()
