package com.babestudios.base.kotlin.ext

import korlibs.time.Date
import korlibs.time.DateException
import korlibs.time.DateFormat
import korlibs.time.format
import korlibs.time.parseDate
import org.lighthousegames.logging.logging

/**
 * These extensions use Klock:
 * https://docs.korge.org/klock/
 *
 * They are just simple convenience extensions copied and adapted from an earlier class and extension collection.
 *
 * Generally we might need three kind of results converted from the other two: Date (or DateTime), formatted String and
 * timestamp long (either ms or sec), but for simplicity, we convert here between String and Long. For Date and
 * DateTime convert it directly or one of the above.
 */

val ISO_DATE : DateFormat
	get() = DateFormat("yyyy-MM-dd")

val SHORT_DATE : DateFormat
	get() = DateFormat("d MMM yyyy")

val SHORT_DATE_WITH_SLASHES : DateFormat
	get() = DateFormat("dd/MMM/yyyy")

val SHORT_TIME : DateFormat
	get() = DateFormat("HH:mm:ss")

val SIMPLE_DATE_TIME : DateFormat
	get() = DateFormat("yyyy-MM-dd HH:mm")

/**
 * From Unix ms to String. Default format: "dd MMM yyyy"
 */
fun Long.toDateString(dateFormat: DateFormat = SHORT_DATE): String {
	return dateFormat.format(this)
}

fun Long.toTimeString(dateFormat: DateFormat = SHORT_TIME): String {
	return dateFormat.format(this)
}

fun String.parseDate(dateFormat: DateFormat = ISO_DATE): Long? {
	return try {
		dateFormat.parseDate(this).dateTimeDayStart.unixMillisLong
	} catch (e: DateException) {
		logging().e { e.message }
		null
	}
}

const val MILLIS = 1_000

fun Date.convertToTimestampSeconds(): Long {
	return this.dateTimeDayStart.unixMillisLong / MILLIS
}
