package com.babestudios.base.kotlin.io

const val RESOURCE_PATH = "./src/commonTest/resources"

expect fun readCommonResource(
	resourceName: String,
): String

expect fun readResource(
	resourceName: String,
): String
