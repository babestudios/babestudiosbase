@file:JvmName("readBinaryResourceJvm") //https://youtrack.jetbrains.com/issue/KT-21186
package com.babestudios.base.kotlin.io

import java.io.File

/**
 * TODO Explain this. Hard coding is probably not good, but the param being the full path could be fine?!
 */
actual fun readCommonResource( resourceName: String): String =
	File("$RESOURCE_PATH/$resourceName").readText()

actual fun readResource(resourceName: String): String =
	String(ClassLoader.getSystemResourceAsStream(resourceName)!!.readBytes())
