plugins {
	kotlin("multiplatform")
	id("com.android.library")
	id("maven-publish")
}

@OptIn(org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi::class)
kotlin {
	applyDefaultHierarchyTemplate()

	androidTarget {

		compilations.all {
			kotlinOptions {
				jvmTarget = "11"
			}
		}

		publishLibraryVariants("release")

	}

	listOf(
		iosX64(),
		iosArm64(),
		iosSimulatorArm64()
	).forEach {
		it.binaries.framework {
			baseName = "base-kotlin"
		}
	}

	sourceSets {
		val commonMain by getting {
			dependencies {
				api("com.soywiz.korlibs.klock:klock:4.0.9")
				implementation("io.insert-koin:koin-core:3.4.3")
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
				implementation("org.lighthousegames:logging:1.3.0")
			}
		}
		val androidMain by getting {
			dependencies {
				api("com.soywiz.korlibs.klock:klock:4.0.9")
			}
		}
		val commonTest by getting {
			dependencies {
				implementation("junit:junit:4.13.2")
				implementation(kotlin("test"))
				implementation("io.kotest:kotest-assertions-core:5.6.2")
			}
		}
	}
}

android {
	namespace = "com.babestudios.base.kotlin"
	compileSdk = 34
	defaultConfig {
		minSdk = 21
	}

	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_11
		targetCompatibility = JavaVersion.VERSION_11
	}
}
