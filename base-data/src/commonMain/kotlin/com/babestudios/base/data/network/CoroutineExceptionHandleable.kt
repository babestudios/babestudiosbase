package com.babestudios.base.data.network

import io.ktor.client.call.body
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.serialization.json.JsonObject

interface CoroutineExceptionHandleable {

	/**
	 * Use this when there is an HttpException and the error is sent inside the [HttpResponse], which can be
	 * retrieved from [HttpResponse.body].
	 * @returns 1. Content of error field 2. Content of message field 3. A fallback string
	 * THIS WAS NOT TESTED, when it was converted to kotlinx.serialization, so it needs tests when used.
	 */
	suspend fun errorMessageFromResponseBody(responseBody: HttpResponse): String {
		val jsonObject = JsonObject(responseBody.body())
		val error = jsonObject.getOrElse("error") { "" } as String
		return error.ifEmpty { jsonObject.getOrElse("message") { "An error happened, try again." } as String }
	}

	/**
	 * Use this if the error is embedded in a 200 OK response and you want to trow an error.
	 * This can happen with e.g. GraphQL Apollo or whenever the back end dev thinks it's a good idea.
	 */
	fun errorMessageFromResponseObject(errorObject: Any?): String? = ""

	/**
	 * Implement this for each project to have a generic error resolver, that can be added to each network call
	 * through a compose operator.
	 * You could use onErrorReturn to transform a HttpException with [errorMessageFromResponseBody] (be careful not
	 * to throw the error(will only add the new one), but use Single.error<T> or similar), or a map operator to
	 * transform an embedded error with [errorMessageFromResponseObject].
	 *
	 * Add other reactive patterns (Observable, Flowable, etc. as needed)
	 */
	val exceptionHandler: CoroutineExceptionHandler

}
