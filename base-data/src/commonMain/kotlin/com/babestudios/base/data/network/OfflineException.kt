package com.babestudios.base.data.network

/**
 * This exception can be used in conjunction with local storage to deliver saved results in case of
 * Offline mode (when we check this before the network call) or
 * with [com.babestudios.base.data.network.CoroutineExceptionHandleable] when we catch an error.
 */
class OfflineException(val obj: Any): Exception()
