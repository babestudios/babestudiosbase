plugins {
	kotlin("multiplatform")
	id("com.android.library")
	id("maven-publish")
}

@OptIn(org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi::class)
kotlin {
	targetHierarchy.default()

	android {

		compilations.all {
			kotlinOptions {
				jvmTarget = "11"
			}
		}

		publishLibraryVariants("release")

	}

	listOf(
		iosX64(),
		iosArm64(),
		iosSimulatorArm64()
	).forEach {
		it.binaries.framework {
			baseName = "base-data"
		}
	}

	sourceSets {
		@Suppress("UNUSED_VARIABLE") val commonMain by getting {
			dependencies {
				//implementation("com.squareup.okhttp3:okhttp:4.11.0")
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.2")
				//implementation ("org.json:json:20230227")
				implementation ("org.jetbrains.kotlinx:kotlinx-serialization-core:1.5.1")
				implementation ("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.1")
				implementation ("io.ktor:ktor-client-core:2.3.2")
			}
		}
		@Suppress("UNUSED_VARIABLE") val androidMain by getting {
			dependencies {

			}
		}
		@Suppress("UNUSED_VARIABLE") val commonTest by getting {
			dependencies {

			}
		}
	}
}

android {
	namespace = "com.babestudios.base.data"
	compileSdk = 34
	defaultConfig {
		minSdk = 21
	}

	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_11
		targetCompatibility = JavaVersion.VERSION_11
	}
}
